### What are these scripts for? ###

* In order to use these scripts on Git Bash, you need to enable UNIX Tools for Bash during install Git for Windows installation
* By doing this, default Windows find and sort will be overriden by binutils'
* If you've performed default installation, reinstall and choose Unix command line tools on Git for Windows
* Add the scripts path to your PATH for use in Git for Windows
* It requires any functional build from http://www.ffmpeg.org/ for the target system (static build recommended for Windows)
