#!/bin/bash

PROJECT_ELF_FILE=$1
GNU_ARM_PATH=/c/Renesas/Synergy/e2studio_v6.2.0_ssp_v1.4.0/toolchains/gcc_arm/4.9_2015q3/bin/
GNU_ARM_NM_UTILITY=arm-none-eabi-nm

printf "Listing project RAM objects by size...\n\n"
printf "RAM objects sizes:\n"
${GNU_ARM_PATH}${GNU_ARM_NM_UTILITY} --size-sort ${PROJECT_ELF_FILE} | grep -e ' b ' 
printf "\nTotal Objects: " 
${GNU_ARM_PATH}${GNU_ARM_NM_UTILITY} --size-sort ${PROJECT_ELF_FILE} | grep -e ' b ' | wc -l | tr -d '\n'
printf "\t\tTotal RAM usage: "
${GNU_ARM_PATH}${GNU_ARM_NM_UTILITY} --size-sort ${PROJECT_ELF_FILE} | grep -e ' b '  | cut -d ' ' -f 1 | gawk '$1="0x"$1' | gawk --non-decimal-data '{sum+=$1} END {print sum " bytes"}'