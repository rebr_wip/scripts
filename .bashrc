#!/bin/bash

if [ ${HOSTNAME} = "BRAVAWN" ]; then
WS_ROOT="/d"
QT_VERSION=5.10.1
else
WS_ROOT="/c"
QT_VERSION=5.9.1
fi

GIT_WS_ROOT="${WS_ROOT}/WorkspaceGIT"
SCRIPTS_PATH="${GIT_WS_ROOT}/scripts"

MINGW32_PATH="${WS_ROOT}/Qt/Qt${QT_VERSION}/${QT_VERSION}/mingw53_32/bin"
MINGW32_TOOLS_PATH="${WS_ROOT}/Qt/Qt${QT_VERSION}/${QT_VERSION}/Tools/mingw530_32/bin"

FFMPEG_PATH=${WS_ROOT}"/ffmpeg/bin"
FFMPEG=${FFMPEG_PATH}"/ffmpeg -hide_banner "

FFPROBE=${FFMPEG_PATH}"/ffprobe -v quiet -show_format"

PYTHON2_PATH="/c/Python27/Scripts"

export PATH=$PATH:${SCRIPTS_PATH}:${MINGW32_PATH}:${MINGW32_TOOLS_PATH}:${FFMPEG_PATH}:${PYTHON2_PATH}

#cd $GIT_WS_ROOT

alias cdgit='cd $GIT_WS_ROOT' 
alias rmall='rm -rfv'
alias rm='rm -v'
alias mv='mv -v'
alias cp='cp -v'
alias ping='ping -t' 

CLEAN_MPEG_METADATA_PARAM="-metadata title="" -metadata artist="" -metadata creation_time="" -metadata handler_name="" -metadata encoder="" "

reencode_x265() {
	INPUT=$1
	if [ ${#INPUT} -gt 4 ]; then
	${FFMPEG} -i "$1"  -c:v libx265 -c:a copy -preset medium -tune psnr "${INPUT:0:-4}[x265].mp4"
else
	echo "Invalid input file"
	fi
}

reencode_x265_2pass() {
	INPUT=$1
	if [ ${#INPUT} -gt 4 ]; then
	OUTPUT="${INPUT:0:-4}[x265].mp4"
	${FFMPEG} -i "${INPUT}" -c:v libx265 -b:v 2600k -x265-params pass=1 -c:a aac -b:a 128k -f mp4 /dev/null && \
	${FFMPEG} -i "${INPUT}" -c:v libx265 -b:v 2600k -x265-params pass=2 -c:a aac -b:a 128k "${OUTPUT}"
else
	echo "Invalid input file"
	fi
}

reencode_x264_remove_metadata() {
	INPUT=$1
	if [ ${#INPUT} -gt 4 ]; then
	${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM} -c:v copy -c:a copy "${INPUT:0:-4}[x264].mp4"
else
	echo "Invalid input file"
	fi
}

reencode_x264() {
	INPUT=$1
	if [ ${#INPUT} -gt 4 ]; then
	#${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM} -b:v 2M -maxrate 2M -bufsize 1M "${INPUT:0:-4}[x264].mp4"
	${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM} -preset veryslow -crf 28 "${INPUT:0:-4}[x264].mp4"
else
	echo "Invalid input file"
	fi
}

reencode_x264_size() {
	INPUT=$1
	TARGET_SIZE=$2
	TARGET_AUDIO=$3
	TARGET_SIZE_B=$((${TARGET_SIZE}*1024))
	TARGET_AUDIO_B=$((${TARGET_AUDIO}*1000))
#	echo "TARGET_SIZE_B: " $TARGET_SIZE_B
	SIZE=$(${FFPROBE} ${INPUT} | grep size | cut -f 2 -d '=')
#	echo "size:" $SIZE
    DURATION=$(${FFPROBE} ${INPUT} | grep duration | cut -f 2 -d '=' | cut -f 1 -d '.')
#	echo "duration:" $DURATION
	BITRATE=$((((${TARGET_SIZE_B} * 8192 / ${DURATION})-${TARGET_AUDIO_B})))
#	echo "BITRATE: " $BITRATE
	if [ ${#INPUT} -gt 4 ]; then
#	${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM} -b:v 2M -maxrate 2M -bufsize 1M "${INPUT:0:-4}[x264].mp4"
	${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM} -preset veryslow -b:v ${BITRATE} -maxrate $((2*${BITRATE})) -bufsize $((${BITRATE}/2)) -b:a ${TARGET_AUDIO_B} "${INPUT:0:-4}[x264].mp4"
else
	echo "Invalid input file"
	fi
}

reencode_x264_2pass() {
	INPUT=$1
	if [ ${#INPUT} -gt 4 ]; then
	OUTPUT="${INPUT:0:-4}[x264].mp4"
	${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM}  -codec:v libx264 -b:v 250k -maxrate 450k -bufsize 100k -tune zerolatency -profile:v main -preset veryslow -threads 0 -pass 1 -an -f mp4 -y /dev/null
	${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM}  -codec:v libx264 -b:v 250k -maxrate 450k -bufsize 100k -tune zerolatency -profile:v main -preset veryslow -threads 0 -pass 2 -acodec copy -movflags +faststart -y "${OUTPUT}"
else
	echo "Invalid input file"
	fi
}

reencode_x264_50percent() {
	INPUT=$1
	if [ ${#INPUT} -gt 4 ]; then
	${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM} -preset veryslow -vf scale=iw/2:-2 -c:a aac -b:a 96k "${INPUT:0:-4}[x264].mp4"
else
	echo "Invalid input file"
	fi
}

reencode_x264_webex() {
	INPUT=$1
	if [ ${#INPUT} -gt 4 ]; then
	#${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM} -r 10 -c:a aac -b:a 64k -ac 1 "${INPUT:0:-4}[x264].mp4"
	${FFMPEG} -i "$1" ${CLEAN_MPEG_METADATA_PARAM} -tune stillimage -c:a aac -b:a 64k -ac 1 "${INPUT:0:-4}[x264].mp4"
else
	echo "Invalid input file"
	fi
}

reencode_x264_all() {
	for inp in *.mp4; do
	reencode_x264 "${inp}"
	done
}

extract_audio_low_quality() {
INPUT="$1"
OUTPUT="${INPUT:0:-4}.mp3"
${FFMPEG} -i ${INPUT} -q:a 9 -ar 22050 -map a -y ${OUTPUT}
}

convert_to_gif() {
INPUT="$1"
OUTPUT="${INPUT:0:-4}.gif"
palette="${INPUT:0:-4}.png"
filters="fps=15,scale=320:-1:flags=lanczos"
${FFMPEG} -v warning -i ${INPUT} -vf "$filters,palettegen=stats_mode=diff" -y $palette
#${FFMPEG} -i ${INPUT} -i $palette -lavfi "$filters,paletteuse=dither=none:diff_mode=rectangle" -y ${OUTPUT}
${FFMPEG} -i ${INPUT} -i $palette -lavfi "$filters,paletteuse=dither=bayer:bayer_scale=5:diff_mode=rectangle" -y ${OUTPUT}
rm ${palette}
}

correct_pdsc ()
{
rm *.pdsc;
for packs in *.pack;
do
CURRENT_SSP_VERSION=${packs: -10:5};
/c/Program\ Files/7-zip/7z e $packs *.pdsc;
sed -i 's/\t//g' *.pdsc;
sed -i 's/Tempororary/Temporary/g' *.pdsc;
sed -i 's/--&gt;$//g' *.pdsc;
sed -i 's/^  --$//g' *.pdsc;
sed -i 's/Cvariant=\"\" //g' *.pdsc;
sed -i "s/<description>CUSTOM Board Support Files<\/description>/<description>CUSTOM Board Support Files<\/description>\n            <files>\n                <file category=\"header\" condition=\"\" name=\"src\\\board\\\custom\\\bsp.h\" select=\"\" version=\"$CURRENT_SSP_VERSION\" \/>\n                <file category=\"source\" condition=\"\" name=\"src\\\board\\\custom\\\bsp.c\" select=\"\" version=\"$CURRENT_SSP_VERSION\" \/>\n            <\/files>/g" *.pdsc;
sed -i "s/<component Cclass=\"Documentation\" Cgroup=\"SSP\" Csub=\"all\" Cvendor=\"Renesas\" Cversion=\"$CURRENT_SSP_VERSION\" condition=\"\">/<component Cclass=\"Documentation\" Cgroup=\"SSP\" Csub=\"all\" Cvendor=\"Renesas\" Cversion=\"$CURRENT_SSP_VERSION\" condition=\"\">\n        <description>Documentation<\/description>/g" *.pdsc;
/c/Program\ Files/7-zip/7z u -aoa $packs *.pdsc;
rm *.pdsc;
done
}
