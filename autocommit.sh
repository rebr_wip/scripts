#!/bin/bash
while [[ $# -gt 1 ]]
do
key="$1"

case $key in
    -m|--message)
    MESSAGE="$2"
    shift # past argument
    ;;
    -b|--branch)
    BRANCH="$2"
    shift # past argument
    ;;
    -h|--help)
    echo "Auto Commit Script"
    echo " -m | --message   : Commit Message (mandatory)"
    echo " -b | --branch    : select branch to commit"
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

if [[ -z ${MESSAGE} ]]; then
    echo "You need to specify a message for proper commit."
    exit 1;
fi

if [[ -z ${BRANCH} ]]; then
    BRANCH=master
fi

echo MESSAGE         = "${MESSAGE}"
echo BRANCH          = "${BRANCH}"

echo Cleaning project tree...
echo ========================
clean_projects.sh
echo Adding modified files...
echo ========================
git add .
echo Commiting to the "${BRANCH}" branch
echo ========================
git commit -m "${MESSAGE}"
echo Pushing commit to the origin on the "${BRANCH}" branch
echo ========================
git push -u origin $2
