#!/bin/bash
find -type d | grep ja$ | xargs rm -rfv
find -type d | grep DefaultBuild$ | xargs rm -rfv
find -type d | grep HardwareDebug$ | xargs rm -rfv
find -type d | grep Debug$ | xargs rm -rfv
find -type d | grep Release$ | xargs rm -rfv
find -type d | grep .module_descriptions$ | xargs rm -rfv
find -type d | grep synergy$ | xargs rm -rfv
find -type d | grep synergy_gen$ | xargs rm -rfv
