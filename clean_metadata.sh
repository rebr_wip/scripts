#!/bin/bash
find -type d | grep libhover$ | xargs rm -rfv
find -type d | grep history$  | xargs rm -rfv
find .       | grep pdom$     | xargs rm -fv
find .       | grep .bak_     | xargs rm -fv
